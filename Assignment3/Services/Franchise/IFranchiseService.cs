﻿using Assignment3.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Assignment3.Services
{
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        public Task<Franchise> GetSpecificFranchiseAsync(int id);
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseAsync(Franchise franchise);
        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExists(int id);
        public Task UpdateMoviesInFranchiseAsync(int franchiseId, int[] movieIds);
        // Reporting
        public Task<IEnumerable<Character>> GetCharactersInFranchise(int id);
        public Task<IEnumerable<Movie>> GetMoviesInFranchise(int id);

    }
}
