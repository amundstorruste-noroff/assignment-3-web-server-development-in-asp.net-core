﻿using Assignment3.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDbContext _context;

        public FranchiseService(MovieDbContext context)
        {
            _context = context;
        }
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(f => f.Id == id);
        }

        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises.ToListAsync();
        }

        public async Task<Franchise> GetSpecificFranchiseAsync(int id)
        {
            return await _context.Franchises.FindAsync(id);
        }

        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            if (FranchiseExists(franchise.Id)) _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateMoviesInFranchiseAsync(int franchiseId, int[] movieIds)
        {
            if (FranchiseExists(franchiseId) is false) return;

            foreach(var movieId in movieIds) {
                var movie = await _context.Movies.FindAsync(movieId);
                if (movie is null) continue;

                movie.FranchiseId = franchiseId;
                _context.Entry(movie).State = EntityState.Modified;
            }
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Character>> GetCharactersInFranchise(int id)
        {
            var movies = await GetMoviesInFranchise(id);
            List<Character> characters = new();
            foreach (var movie in movies)
            {
                characters.AddRange(movie.Characters);
            }
            return characters;
        }

        public async Task<IEnumerable<Movie>> GetMoviesInFranchise(int id)
        {
            var movies = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.FranchiseId == id)
                .ToArrayAsync();
            return movies;
        }
    }
}
