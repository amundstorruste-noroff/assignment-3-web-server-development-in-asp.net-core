﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment3.Models
{
    [Table("Characters")]
    public class Character
    {
        // Private key
        public int Id { get; set; }
        
        //Fields
        [Required]
        [MaxLength(100)]
        public string FullName { get; set; }
        [MaxLength(100)]
        public string Alias { get; set; }
        [Required]
        [MaxLength(10)]
        public string Gender { get; set; }
        public string Picture { get; set; }

        // Relationships
        public ICollection<Movie> Movies { get; set; }
    }
}
