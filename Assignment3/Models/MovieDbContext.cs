﻿using Assignment3.DefaultData;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Assignment3.Models
{
    public class MovieDbContext : DbContext
    {
        // Constructor
        public MovieDbContext(DbContextOptions<MovieDbContext> options) : base(options) { }

        // Tables
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }

        // Data seeding
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Generate default Franchises, Movies, and Characters
            modelBuilder.Entity<Franchise>().HasData(DefaultFranchises.GetFranchises());
            modelBuilder.Entity<Movie>().HasData(DefaultMovies.GetMovies());
            modelBuilder.Entity<Character>().HasData(DefaultCharacters.GetCharacters());

            // Explicitly define Character-Movie many-to-many relationsship and seed data
            modelBuilder.Entity<Movie>()
                .HasMany(m => m.Characters)
                .WithMany(c => c.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "CharactersInMovies",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("MovieId", "CharacterId");
                        // Seed many-to-many relationship for Default Characters in Movies
                        je.HasData(
                            // Harry potter: Id 1
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 1, MovieId = 2 },
                            new { CharacterId = 1, MovieId = 3 },
                            new { CharacterId = 1, MovieId = 4 },
                            new { CharacterId = 1, MovieId = 5 },
                            new { CharacterId = 1, MovieId = 6 },
                            new { CharacterId = 1, MovieId = 7 },
                            new { CharacterId = 1, MovieId = 8 },
                            // Albus Dumbledore: Id 2
                            new { CharacterId = 2, MovieId = 1 },
                            new { CharacterId = 2, MovieId = 2 },
                            new { CharacterId = 2, MovieId = 3 },
                            new { CharacterId = 2, MovieId = 4 },
                            new { CharacterId = 2, MovieId = 5 },
                            new { CharacterId = 2, MovieId = 6 },
                            new { CharacterId = 2, MovieId = 7 },
                            new { CharacterId = 2, MovieId = 8 },
                            // Newt Scamander: Id 3
                            new { CharacterId = 3, MovieId = 9 },
                            // Gellert Grindewald: Id 4
                            new { CharacterId = 4, MovieId = 7 },
                            new { CharacterId = 4, MovieId = 9 },
                            // Jack Sparrow: Id 5
                            new { CharacterId = 5, MovieId = 10 }
                         );
                    });
        }
    }
}
