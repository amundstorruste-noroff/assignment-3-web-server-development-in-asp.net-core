﻿namespace Assignment3.Models.DTO.FranchiseDTOs
{
    public class FranchiseEditDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
