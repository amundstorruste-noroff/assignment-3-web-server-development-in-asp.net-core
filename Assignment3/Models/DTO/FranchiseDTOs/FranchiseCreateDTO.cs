﻿namespace Assignment3.Models.DTO.FranchiseDTOs
{
    public class FranchiseCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
