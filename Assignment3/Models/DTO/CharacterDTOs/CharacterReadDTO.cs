﻿using System.Collections.Generic;

namespace Assignment3.Models.DTO.CharacterDTOs
{
    public class CharacterReadDTO
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
        // Relationships
        public List<int> Movies { get; set; }
    }
}
