﻿namespace Assignment3.Models.DTO.CharacterDTOs
{
    public class CharacterCreateDTO
    {
        public string FullName { get; set; }
        public string Gender { get; set; }
    }
}
