﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3.Models;
using System.Net.Mime;
using AutoMapper;
using Assignment3.Models.DTO.MovieDTOs;
using Assignment3.Services;
using Assignment3.Models.DTO.CharacterDTOs;

namespace Assignment3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;

        public MoviesController(IMovieService movieService, IMapper mapper)
        {
            _movieService = movieService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all Movies stored in the database.
        /// </summary>
        /// <returns></returns>
        // GET: api/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            var movies = await _movieService.GetAllMoviesAsync();
            var moviesDTO = _mapper.Map<List<MovieReadDTO>>(movies);
            return moviesDTO;
        }

        /// <summary>
        /// Get a single Movie from the database, identified by the MovieId.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _movieService.GetSpecificMovieAsync(id);

            if (movie == null)
            {
                return NotFound("Movie not found.");
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Update an existing Movie with new data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieDTO"></param>
        /// <returns></returns>
        // PUT: api/Movies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movieDTO)
        {
            if (id != movieDTO.Id)
            {
                return BadRequest();
            }

            if (!_movieService.MovieExists(id)) return NotFound();

            var movie = _mapper.Map<Movie>(movieDTO);
            await _movieService.UpdateMovieAsync(movie);

            return NoContent();
        }

        /// <summary>
        /// Create a new Movie and store in database.
        /// </summary>
        /// <param name="movieDTO"></param>
        /// <returns></returns>
        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<MovieCreateDTO>> PostMovie(MovieCreateDTO movieDTO)
        {
            var domainMovie= _mapper.Map<Movie>(movieDTO);
            domainMovie = await _movieService.AddMovieAsync(domainMovie);

            return CreatedAtAction("GetMovie", new { id = domainMovie.Id }, movieDTO);
        }

        /// <summary>
        /// Delete and existing Movie.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id)) return NotFound();

            await _movieService.DeleteMovieAsync(id);

            return NoContent();
        }

        /// <summary>
        /// Update which Characters are in a Movie from a list of Character Ids.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieIds"></param>
        /// <returns></returns>
        [HttpPut("{id}/UpdateCharacters")]
        public async Task<IActionResult> UpdateCharacters(int id, int[] movieIds)
        {
            if (!_movieService.MovieExists(id)) return NotFound();
            await _movieService.UpdateCharactersInMovieAsync(id, movieIds);
            return NoContent();
        }

        /// <summary>
        /// Returns all Characters that play a role in a Movie.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/GetCharactersInMovie")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInMovie(int id)
        {
            var characters = await _movieService.GetCharactersInMovie(id);
            var charactersDTO = _mapper.Map<List<CharacterReadDTO>>(characters);
            return charactersDTO;
        }

    }
}
