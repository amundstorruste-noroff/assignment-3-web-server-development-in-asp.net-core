﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3.Models;
using AutoMapper;
using Assignment3.Models.DTO.CharacterDTOs;
using System.Net.Mime;
using Assignment3.Services;

namespace Assignment3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;

        public CharactersController(ICharacterService characterService, IMapper mapper)
        {
            _characterService = characterService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all Characters stored in the database.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            var characters = await _characterService.GetAllCharactersAsync();
            var charactersDTO = _mapper.Map<List<CharacterReadDTO>>(characters);
            return charactersDTO;
        }

        /// <summary>
        /// Returns a single character from the database, identified by the CharacterId.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _characterService.GetSpecificCharacterAsync(id);

            if (character == null)
            {
                return NotFound();
            }
            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Update an existing Character with new data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterDTO"></param>
        /// <returns></returns>
        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO characterDTO)
        {
            if (id != characterDTO.Id)
            {
                return BadRequest();
            }

            if (!_characterService.CharacterExists(id)) return NotFound();

            var character = _mapper.Map<Character>(characterDTO);
            await _characterService.UpdateCharacterAsync(character);

            return NoContent();
        }

        /// <summary>
        /// Create a new Character and store in database.
        /// </summary>
        /// <param name="characterDTO"></param>
        /// <returns></returns>
        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CharacterCreateDTO>> PostCharacter(CharacterCreateDTO characterDTO)
        {
            var domainCharacter = _mapper.Map<Character>(characterDTO);
            domainCharacter = await _characterService.AddCharacterAsync(domainCharacter);

            return CreatedAtAction("GetCharacter", new { id = domainCharacter.Id }, characterDTO);
        }

        /// <summary>
        /// Delete an existing Character.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_characterService.CharacterExists(id)) return NotFound();

            await _characterService.DeleteCharacterAsync(id);

            return NoContent();
        }
    }
}
