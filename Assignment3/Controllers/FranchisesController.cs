﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment3.Models;
using System.Net.Mime;
using AutoMapper;
using Assignment3.Models.DTO.FranchiseDTOs;
using Assignment3.Services;
using Assignment3.Models.DTO.MovieDTOs;
using Assignment3.Models.DTO.CharacterDTOs;


namespace Assignment3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;

        public FranchisesController(IFranchiseService franchiseService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all Franchises stored in the database.
        /// </summary>
        /// <returns></returns>
        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            var franchises = await _franchiseService.GetAllFranchisesAsync();
            var franchisesDTO = _mapper.Map<List<FranchiseReadDTO>>(franchises);
            return franchisesDTO;
        }

        /// <summary>
        /// Get a single Franchise from the database, identifyed by the FranchiseId.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _franchiseService.GetSpecificFranchiseAsync(id);
            
            if (franchise == null)
            {
                return NotFound("Franchise not found.");
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Update an existing Franchise with new data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchiseDTO"></param>
        /// <returns></returns>
        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchiseDTO)
        {
            if (id != franchiseDTO.Id)
            {
                return BadRequest();
            }

            if (!_franchiseService.FranchiseExists(id)) return NotFound();

            var franchise = _mapper.Map<Franchise>(franchiseDTO);
            await _franchiseService.UpdateFranchiseAsync(franchise);

            return NoContent();
        }

        /// <summary>
        /// Create a new Franchise and store in database.
        /// </summary>
        /// <param name="franchiseDTO"></param>
        /// <returns></returns>
        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<FranchiseCreateDTO>> AddFranchise(FranchiseCreateDTO franchiseDTO)
        {
            var domainFranchise = _mapper.Map<Franchise>(franchiseDTO);
            domainFranchise = await _franchiseService.AddFranchiseAsync(domainFranchise);

            return CreatedAtAction("GetFranchise", new { id = domainFranchise.Id }, franchiseDTO);
        }

        /// <summary>
        /// Delete an existing Franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id)) return NotFound();

            await _franchiseService.DeleteFranchiseAsync(id);

            return NoContent();
        }

        /// <summary>
        /// Update which Movies are in a Franchise from a list of Movie Ids.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieIds"></param>
        /// <returns></returns>
        [HttpPut("{id}/UpdateMovies")]
        public async Task<IActionResult> UpdateMovies(int id, int[] movieIds)
        {
            if (!_franchiseService.FranchiseExists(id)) return NotFound();
            await _franchiseService.UpdateMoviesInFranchiseAsync(id, movieIds);
            return NoContent();
        }

        /// <summary>
        /// Returns a list of all Movies in a Franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/GetMoviesInFranchise")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesInFranchise(int id)
        {
            var movies = await _franchiseService.GetMoviesInFranchise(id);
            var moviesDTO = _mapper.Map<List<MovieReadDTO>>(movies);
            return moviesDTO;
        }

        /// <summary>
        /// Returns all Characters that play role in a Movie in a Franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/GetCharactersInFranchise")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInFranchise(int id)
        {
            var characters = await _franchiseService.GetCharactersInFranchise(id);
            var charactersDTO = _mapper.Map<List<CharacterReadDTO>>(characters);
            return charactersDTO;
        }

    }
}
