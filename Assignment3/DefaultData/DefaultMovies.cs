﻿using Assignment3.Models;
using System.Collections.Generic;

namespace Assignment3.DefaultData
{
    public class DefaultMovies
    {
        public static IEnumerable<Movie> GetMovies()
        {
            IEnumerable<Movie> movieList = new List<Movie>()
            {
                new Movie
                {
                    Id = 1,
                    Title = "Harry Potter and the Philosopher's Stone",
                    Genre = "Fantasy",
                    ReleaseYear = 2001,
                    Director = "Chris Columbus",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/7/7a/Harry_Potter_and_the_Philosopher%27s_Stone_banner.jpg",
                    Trailer = @"https://www.youtube.com/watch?v=VyHV0BRtdxo",
                    FranchiseId = 1,
                },
                new Movie
                {
                    Id = 2,
                    Title = "Harry Potter and the Chamber of Secrets",
                    Genre = "Fantasy",
                    ReleaseYear = 2002,
                    Director = "Chris Columbus",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/c/c0/Harry_Potter_and_the_Chamber_of_Secrets_movie.jpg",
                    Trailer = @"https://www.youtube.com/watch?v=1bq0qff4iF8",
                    FranchiseId = 1,
                },
                new Movie
                {
                    Id = 3,
                    Title = "Harry Potter and the Prisoner of Azkaban",
                    Genre = "Fantasy",
                    ReleaseYear = 2004,
                    Director = "Alfonso Cuarón",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/b/bc/Prisoner_of_azkaban_UK_poster.jpg",
                    Trailer = @"https://www.youtube.com/watch?v=1ZdlAg3j8nI",
                    FranchiseId = 1,
                },
                new Movie
                {
                    Id = 4,
                    Title = "Harry Potter and the Goblet of Fire",
                    Genre = "Fantasy",
                    ReleaseYear = 2005,
                    Director = "Mike Newell",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/c/c9/Harry_Potter_and_the_Goblet_of_Fire_Poster.jpg",
                    Trailer = @"https://www.youtube.com/watch?v=3EGojp4Hh6I",
                    FranchiseId = 1,
                },
                new Movie
                {
                    Id = 5,
                    Title = "Harry Potter and the Order of the Phoenix",
                    Genre = "Fantasy",
                    ReleaseYear = 2007,
                    Director = "David Yates",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/e/e7/Harry_Potter_and_the_Order_of_the_Phoenix_poster.jpg",
                    Trailer = @"https://www.youtube.com/watch?v=y6ZW7KXaXYk",
                    FranchiseId = 1,
                },
                new Movie
                {
                    Id = 6,
                    Title = "Harry Potter and the Half-Blood Prince",
                    Genre = "Fantasy",
                    ReleaseYear = 2009,
                    Director = "David Yates",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/3/3f/Harry_Potter_and_the_Half-Blood_Prince_poster.jpg",
                    Trailer = @"https://www.youtube.com/watch?v=0ubLa8s0YZk",
                    FranchiseId = 1,
                },
                new Movie
                {
                    Id = 7,
                    Title = "Harry Potter and the Deathly Hallows – Part 1",
                    Genre = "Fantasy",
                    ReleaseYear = 2010,
                    Director = "David Yates",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/2/2d/Harry_Potter_and_the_Deathly_Hallows_%E2%80%93_Part_1.jpg",
                    Trailer = @"https://www.youtube.com/watch?v=MxqsmsA8y5k",
                    FranchiseId = 1,
                },
                new Movie
                {
                    Id = 8,
                    Title = "Harry Potter and the Deathly Hallows – Part 2",
                    Genre = "Fantasy",ReleaseYear = 2011,Director = "David Yates",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/d/df/Harry_Potter_and_the_Deathly_Hallows_%E2%80%93_Part_2.jpg",
                    Trailer = @"https://www.youtube.com/watch?v=5NYt1qirBWg",
                    FranchiseId = 1,
                },
                new Movie{
                    Id = 9,
                    Title = "Fantastic Beasts and Where To Find Them",
                    Genre = "Fantasy",
                    ReleaseYear = 2016,
                    Director = "David Yates",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/5/5e/Fantastic_Beasts_and_Where_to_Find_Them_poster.png",
                    Trailer = @"https://www.youtube.com/watch?v=ViuDsy7yb8M",
                    FranchiseId = 1,
                },
                new Movie
                {
                    Id = 10,
                    Title = "Pirates of the Caribbean: The Curse of the Black Pearl",
                    Genre = "Fantasy",
                    ReleaseYear = 2003,
                    Director = "Gore Verbinski",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/8/89/Pirates_of_the_Caribbean_-_The_Curse_of_the_Black_Pearl.png",
                    Trailer = @"https://www.youtube.com/watch?v=naQr0uTrH_s",
                    FranchiseId = 2,
                }
            };
            return movieList;
        }
    }
}
