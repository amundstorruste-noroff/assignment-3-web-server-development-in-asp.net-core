﻿using Assignment3.Models;
using System.Collections.Generic;

namespace Assignment3.DefaultData
{
    public class DefaultFranchises
    {
        public static IEnumerable<Franchise> GetFranchises()
        {
            IEnumerable<Franchise> franchiseList = new List<Franchise>() 
            {
                new Franchise {Id = 1, Name = "Harry Potter", Description = "Magic boi learning how to shoot magics and doesn't afraid of anything." },
                new Franchise {Id = 2, Name = "Pirates Of The Caribbean", Description = "A collection of grand adventure on the seven seas. Yarr."}
            };
            return franchiseList;
        }
    }
}
