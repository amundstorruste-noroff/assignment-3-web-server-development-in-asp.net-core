﻿using Assignment3.Models;
using System.Collections.Generic;
using System.Linq;

namespace Assignment3.DefaultData
{
    public class DefaultCharacters
    {
        public static IEnumerable<Character> GetCharacters()
        {
            IEnumerable<Character> CharacterList = new List<Character>()
            {
                new Character()
                {
                    Id = 1,
                    FullName = "Harry James Potter",
                    Alias = "The Boy Who Lived Through A Database Test",
                    Gender = "Male",
                    Picture = @"https://bok365.no/wp-content/uploads/2016/04/Potter-e1459547925913.jpg",
                },
                new Character()
                {
                    Id = 2,
                    FullName = "Albus Dumbledore",
                    Alias = "Supreme Mugwump of the International Confederation of Wizards",
                    Gender = "Male",
                    Picture = @"https://static.wikia.nocookie.net/harrypotter/images/4/40/Albus_Dumbledore_%28HBP_promo%29_3.jpg/revision/latest?cb=20150822232849",
                },

                new Character()
                {
                    Id = 3,
                    FullName = "Newt Scamander",
                    Alias = "Theseus Scamander",
                    Gender = "Male",
                    Picture = @"https://static.wikia.nocookie.net/harrypotter/images/3/36/Newton_Scamander_Profile_crop.png/revision/latest?cb=20190609204955",
                },
                new Character()
                {
                    Id = 4,
                    FullName = "Gellert Grindewald",
                    Alias = "Percival Graves",
                    Gender = "Male",
                    Picture = @"https://static.wikia.nocookie.net/harrypotter/images/7/79/GellertGrindelwald.PNG/revision/latest/scale-to-width-down/338?cb=20181027094911&path-prefix=no",
                },
                new Character()
                {
                    Id = 5,
                    FullName = "Jack Sparrow",
                    Alias = "The best pirate I have ever seen.",
                    Gender = "Male",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/a/a2/Jack_Sparrow_In_Pirates_of_the_Caribbean-_At_World%27s_End.JPG"
                },
                new Character()
                {
                    Id = 6,
                    FullName = "Bootstrap Bill",
                    Alias = "Sunkman.",
                    Gender = "Male",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/a/a2/Jack_Sparrow_In_Pirates_of_the_Caribbean-_At_World%27s_End.JPG"
                },
                new Character()
                {
                    Id = 7,
                    FullName = "Will Turner",
                    Alias = "Son of bootman.",
                    Gender = "Male",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/a/a2/Jack_Sparrow_In_Pirates_of_the_Caribbean-_At_World%27s_End.JPG"
                }
            };
            return CharacterList;
        }
    }
}
