﻿using System.Collections.Generic;

namespace Assignment3.DefaultData
{
    public class DefaultRelationships
    {
        public static List<object> GetCharactersInMovies()
        {
            List<object> CharactersInMovies = new()
            {
                // Harry potter: Id 1
                new { CharacterId = 1, MovieId = 1 },
                new { CharacterId = 1, MovieId = 2 },
                new { CharacterId = 1, MovieId = 3 },
                new { CharacterId = 1, MovieId = 4 },
                new { CharacterId = 1, MovieId = 5 },
                new { CharacterId = 1, MovieId = 6 },
                new { CharacterId = 1, MovieId = 7 },
                new { CharacterId = 1, MovieId = 8 },
                // Albus Dumbledore: Id 2
                new { CharacterId = 2, MovieId = 1 },
                new { CharacterId = 2, MovieId = 2 },
                new { CharacterId = 2, MovieId = 3 },
                new { CharacterId = 2, MovieId = 4 },
                new { CharacterId = 2, MovieId = 5 },
                new { CharacterId = 2, MovieId = 6 },
                new { CharacterId = 2, MovieId = 7 },
                new { CharacterId = 2, MovieId = 8 },
                // Newt Scamander: Id 3
                new { CharacterId = 3, MovieId = 9 },
                // Gellert Grindewald: Id 4
                new { CharacterId = 4, MovieId = 7 },
                new { CharacterId = 4, MovieId = 9 },
                // Jack Sparrow: Id 5
                new { CharacterId = 5, MovieId = 10 },
                // Bootstrap Bill: Id 6
                new { CharacterId = 6, MovieId = 10 },
                // Will Turner: Id 7
                new { CharacterId = 7, MovieId = 10 }

            };
            return CharactersInMovies;
        }
    }
}
