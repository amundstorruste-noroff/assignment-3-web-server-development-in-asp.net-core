﻿using Assignment3.Models;
using Assignment3.Models.DTO.CharacterDTOs;
using AutoMapper;
using System.Linq;

namespace Assignment3.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            // Create mappings for direct subsets
            CreateMap<Character, CharacterCreateDTO>().ReverseMap();
            CreateMap<Character, CharacterEditDTO>().ReverseMap();

            // Manually map List<Movies> to List<Movie.Id> for generic DTO
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(characterDTO => characterDTO.Movies, opt => opt
                // Turn array of related Movies into array of related MovieIds
                .MapFrom(character => character.Movies.Select(movie => movie.Id).ToArray()))
                .ReverseMap();
        }
    }
}
