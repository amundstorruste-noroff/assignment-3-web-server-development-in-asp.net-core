﻿using Assignment3.Models;
using Assignment3.Models.DTO.MovieDTOs;
using AutoMapper;
using System.Linq;

namespace Assignment3.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile() {
        CreateMap<Movie, MovieCreateDTO>().ReverseMap();
        CreateMap<Movie, MovieEditDTO>().ReverseMap();

        // Manually map List<Movies> to List<Movie.Id> for generic DTO
        CreateMap<Movie, MovieReadDTO>()
                .ForMember(movieDTO => movieDTO.Characters, opt => opt
                // Turn array of related Movies into array of related MovieIds
                .MapFrom(movie => movie.Characters.Select(character => character.Id).ToArray()))
                .ReverseMap();
        }
    }
}
