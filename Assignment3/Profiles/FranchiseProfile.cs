﻿using Assignment3.Models;
using Assignment3.Models.DTO.FranchiseDTOs;
using AutoMapper;
using System.Linq;

namespace Assignment3.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            // Create mappings for direct subsets
            CreateMap<Franchise, FranchiseCreateDTO>().ReverseMap();
            CreateMap<Franchise, FranchiseEditDTO>().ReverseMap();
            CreateMap<Franchise, FranchiseReadDTO>().ReverseMap();
        }
    }
}
