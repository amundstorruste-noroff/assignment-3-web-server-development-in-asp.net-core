﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Assignment3.Migrations
{
    public partial class InitialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    ReleaseYear = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CharactersInMovies",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false),
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharactersInMovies", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_CharactersInMovies_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharactersInMovies_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "The Boy Who Lived Through A Database Test", "Harry James Potter", "Male", "https://bok365.no/wp-content/uploads/2016/04/Potter-e1459547925913.jpg" },
                    { 2, "Supreme Mugwump of the International Confederation of Wizards", "Albus Dumbledore", "Male", "https://static.wikia.nocookie.net/harrypotter/images/4/40/Albus_Dumbledore_%28HBP_promo%29_3.jpg/revision/latest?cb=20150822232849" },
                    { 3, "Theseus Scamander", "Newt Scamander", "Male", "https://static.wikia.nocookie.net/harrypotter/images/3/36/Newton_Scamander_Profile_crop.png/revision/latest?cb=20190609204955" },
                    { 4, "Percival Graves", "Gellert Grindewald", "Male", "https://static.wikia.nocookie.net/harrypotter/images/7/79/GellertGrindelwald.PNG/revision/latest/scale-to-width-down/338?cb=20181027094911&path-prefix=no" },
                    { 5, "The best pirate I have ever seen.", "Jack Sparrow", "Male", "https://upload.wikimedia.org/wikipedia/en/a/a2/Jack_Sparrow_In_Pirates_of_the_Caribbean-_At_World%27s_End.JPG" },
                    { 6, "Sunkman.", "Bootstrap Bill", "Male", "https://upload.wikimedia.org/wikipedia/en/a/a2/Jack_Sparrow_In_Pirates_of_the_Caribbean-_At_World%27s_End.JPG" },
                    { 7, "Son of bootman.", "Will Turner", "Male", "https://upload.wikimedia.org/wikipedia/en/a/a2/Jack_Sparrow_In_Pirates_of_the_Caribbean-_At_World%27s_End.JPG" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Magic boi learning how to shoot magics and doesn't afraid of anything.", "Harry Potter" },
                    { 2, "A collection of grand adventure on the seven seas. Yarr.", "Pirates Of The Caribbean" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, "Chris Columbus", 1, "Fantasy", "https://upload.wikimedia.org/wikipedia/en/7/7a/Harry_Potter_and_the_Philosopher%27s_Stone_banner.jpg", 2001, "Harry Potter and the Philosopher's Stone", "https://www.youtube.com/watch?v=VyHV0BRtdxo" },
                    { 2, "Chris Columbus", 1, "Fantasy", "https://upload.wikimedia.org/wikipedia/en/c/c0/Harry_Potter_and_the_Chamber_of_Secrets_movie.jpg", 2002, "Harry Potter and the Chamber of Secrets", "https://www.youtube.com/watch?v=1bq0qff4iF8" },
                    { 3, "Alfonso Cuarón", 1, "Fantasy", "https://upload.wikimedia.org/wikipedia/en/b/bc/Prisoner_of_azkaban_UK_poster.jpg", 2004, "Harry Potter and the Prisoner of Azkaban", "https://www.youtube.com/watch?v=1ZdlAg3j8nI" },
                    { 4, "Mike Newell", 1, "Fantasy", "https://upload.wikimedia.org/wikipedia/en/c/c9/Harry_Potter_and_the_Goblet_of_Fire_Poster.jpg", 2005, "Harry Potter and the Goblet of Fire", "https://www.youtube.com/watch?v=3EGojp4Hh6I" },
                    { 5, "David Yates", 1, "Fantasy", "https://upload.wikimedia.org/wikipedia/en/e/e7/Harry_Potter_and_the_Order_of_the_Phoenix_poster.jpg", 2007, "Harry Potter and the Order of the Phoenix", "https://www.youtube.com/watch?v=y6ZW7KXaXYk" },
                    { 6, "David Yates", 1, "Fantasy", "https://upload.wikimedia.org/wikipedia/en/3/3f/Harry_Potter_and_the_Half-Blood_Prince_poster.jpg", 2009, "Harry Potter and the Half-Blood Prince", "https://www.youtube.com/watch?v=0ubLa8s0YZk" },
                    { 7, "David Yates", 1, "Fantasy", "https://upload.wikimedia.org/wikipedia/en/2/2d/Harry_Potter_and_the_Deathly_Hallows_%E2%80%93_Part_1.jpg", 2010, "Harry Potter and the Deathly Hallows – Part 1", "https://www.youtube.com/watch?v=MxqsmsA8y5k" },
                    { 8, "David Yates", 1, "Fantasy", "https://upload.wikimedia.org/wikipedia/en/d/df/Harry_Potter_and_the_Deathly_Hallows_%E2%80%93_Part_2.jpg", 2011, "Harry Potter and the Deathly Hallows – Part 2", "https://www.youtube.com/watch?v=5NYt1qirBWg" },
                    { 9, "David Yates", 1, "Fantasy", "https://upload.wikimedia.org/wikipedia/en/5/5e/Fantastic_Beasts_and_Where_to_Find_Them_poster.png", 2016, "Fantastic Beasts and Where To Find Them", "https://www.youtube.com/watch?v=ViuDsy7yb8M" },
                    { 10, "Gore Verbinski", 2, "Fantasy", "https://upload.wikimedia.org/wikipedia/en/8/89/Pirates_of_the_Caribbean_-_The_Curse_of_the_Black_Pearl.png", 2003, "Pirates of the Caribbean: The Curse of the Black Pearl", "https://www.youtube.com/watch?v=naQr0uTrH_s" }
                });

            migrationBuilder.InsertData(
                table: "CharactersInMovies",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 3, 9 },
                    { 2, 8 },
                    { 1, 8 },
                    { 4, 7 },
                    { 2, 7 },
                    { 1, 7 },
                    { 2, 6 },
                    { 1, 6 },
                    { 2, 5 },
                    { 1, 5 },
                    { 2, 4 },
                    { 1, 4 },
                    { 2, 3 },
                    { 1, 3 },
                    { 2, 2 },
                    { 1, 2 },
                    { 2, 1 },
                    { 4, 9 },
                    { 5, 10 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharactersInMovies_CharacterId",
                table: "CharactersInMovies",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharactersInMovies");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
